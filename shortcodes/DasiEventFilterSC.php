<?php
//[DasiFilter location="" theme="" venue="" genre="" theme="" pagenum=(int)]
function DasiEventFilter( $atts, $post = "" ){
	$a = shortcode_atts(array(
		'location'			=> '',
		'theme'				=> '',
		'genre'				=> '',
		'start_date'		=> '',
		'end_date'			=> '',
		'pagenum'			=> 1,
		'posts_per_page'	=> 12,
		'show_filter'		=> true
		), $atts);

	if($post != "") {
		$a['pagenum'] 			= $post['dasi_pagenum'];
		$a['location']			= $post['dasi_location'];
		$a['theme']				= $post['dasi_theme'];
		$a['genre']				= $post['dasi_genre'];
		$a['start_date']		= $post['dasi_startdate'];
		$a['end_date']			= $post['dasi_enddate'];
		$a['pagenum']			= $post['dasi_pagenum'];
		$a['posts_per_page']	= $post['dasi_postsperpage'];
	}

	//$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

	$dasi_event_args = array(
		'post_type'			=> 'dasi_event',
		'post_status'		=> 'publish',
		'posts_per_page'	=> $a['posts_per_page'],
		'paged'				=> $a['pagenum'],
		'order'				=> 'ASC',
		'orderby' 			=> 'meta_value',
		'meta_key' 			=> 'dasi_event_first_date'
	);

	//only include IF required
	$dasi_event_args['meta_query']	= array(
		'relation'    => 'AND',
	);

	//LOCATION
	if(!empty($a['location'])) {
		$dasi_event_args['meta_query']['event_location'] =
			[
				"key" 		=> "dasi_event_location",	
				"value" 	=> $a['location'],
				"compare" 	=> "=",
			];
	}


	//THEME FILTER
	if(!empty($a['theme'])) {
		$dasi_event_args['meta_query']['event_theme'] =
			[
				"key" 		=> "dasi_event_theme",	
				"value" 	=> $a['theme'],
				"compare" 	=> "=",
			];

	}


	//GENRE
	if(!empty($a['genre'])) {
		$dasi_event_args['meta_query']['event_genre'] = 
			[
				"key" 		=> "dasi_event_genre",	
				"value" 	=> $a['genre'],
				"compare" 	=> "=",
			];

	}


//LOOK FOR dasi_event_first_date
	// LOGIC FOR DATES!!
	// the events first date should be before the filters end date, meaning it doesnt start before the end date, but may have dates past the end date.
	// the events last date should be after the fitlers start date, meaning it hasnt ended before the filter date(s), but may have started before.


	//DATE BEFORE
	if(!empty($a['start_date'])) {
		// $split_date = explode("-",$a['start_date']);
		// $split_enddate = explode("-",$a['end_date']);
		
		$dasi_event_args['meta_query']['first_date'] =
			[
				"key" 		=> "dasi_event_first_date",	
				"value" 	=> $a['end_date'],
				"compare" 	=> "<=",
				"type"	=> "DATE",
				
			];

		$dasi_event_args['meta_query']['last_date'] =
			[
				"key" 		=> "dasi_event_last_date",	
				"value" 	=> $a['start_date'],
				"compare" 	=> ">=",
				"type"	=> "DATE",
				
			];

	}else{

		//if no event date filter set, just use todays date and go from there.
		
		$dasi_event_args['meta_query']['last_date'] = 
			[
				"key" 		=> "dasi_event_last_date",	
				"value" 	=> date("Y-m-d"),
				"compare" 	=> ">=",
				"type"	=> "DATE",
				
			];


	}

	$dasi_events = new WP_Query($dasi_event_args);
	$dasi_event_output = '';
	if(isset($_POST['dasi_reset_posts'])) {
		if($_POST['dasi_reset_posts'] == true) {

		}
	}

	if($dasi_events->have_posts()) {
		//if its the first time this has ran, include the wrapper.
		if($a['pagenum'] == 1 && !isset($_POST['dasi_reset_posts']) ) {
			if($a['posts_per_page']) {
				echo dasi_event_filter($a['location']);
			}

			$dasi_event_output = $dasi_event_output . '<div
			 class="row event-listing--row dasi-events"
			 id="dasi-events"
			 data-location="'.$a['location'].'"
			 data-genre="'.$a['genre'].'"
			 data-theme="'.$a['theme'].'"
			 data-startdate="'.$a['start_date'].'"
			 data-enddate="'.$a['end_date'].'"
			 data-numposts=""
			 data-postsperpage="'.$a['posts_per_page'].'"
			 data-pagenum="'.$a['pagenum'].'"
			 data-pagesmax="'.$dasi_events->max_num_pages.'">';
		}

		while($dasi_events->have_posts()) {
			$dasi_events->the_post();
			$dasi_event_location  	= get_post_meta(get_the_ID(), 'dasi_event_location',true);
			$dasi_event_genre  		= get_post_meta(get_the_ID(), 'dasi_event_genre',true);
			$dasi_event_first_date 	= date('d-M-Y',strtotime(get_post_meta(get_the_ID(), 'dasi_event_first_date',true)));
			$dasi_event_last_date 	= date('d-M-Y',strtotime(get_post_meta(get_the_ID(), 'dasi_event_last_date',true)));
			$final_date 			= $dasi_event_first_date .' - ' . $dasi_event_last_date;

			if($dasi_event_first_date == $dasi_event_last_date) {
				$final_date = $dasi_event_first_date;
			}

			//get the correct image, and fallback if not found.
			$dasi_event_image = get_the_post_thumbnail_url('featured-small');
			if(empty($dasi_image)) {
				$dasi_event_image = get_the_post_thumbnail_url();
			}

			$dasi_event_output = $dasi_event_output . '<div class="small-12 large-4 column">
		        <div class="card '.sanitize_title($dasi_event_location).' " data-location="'.sanitize_title($dasi_event_location).'">
		        	<div class="card--image" style="background-image: url('.$dasi_event_image .')"></div>
		            <div class="card--desc">
		                <h4 class="card--title">'.get_the_title().'</h4>
		                <p>
		                    ' . $final_date . '
		                    <br />
		                    ' . $dasi_event_location . '
		                </p>
		            	
		                <a class="primary-btn card--btn" href="' . get_permalink() . '">Read More</a>
		            </div>
		        </div><!-- // card -->
		    </div>';
		}//endwhile

		$dasi_max_pages = $dasi_events->max_num_pages;
		if($a['pagenum'] == 1 && !isset($_POST['dasi_reset_posts'])) {
			$dasi_event_output = $dasi_event_output . '</div><!-- // row -->';
		} 
		// don't display the button if there are not enough posts
		if (  $a['pagenum'] == 1 && $dasi_events->max_num_pages > 1 && !isset($_POST['dasi_reset_posts'])) {
			$dasi_event_output = $dasi_event_output . '<div class="row column align-center dasi-load-more-parent text-center"><div class="primary-btn dasi_loadmore text-center">More Events</div></div>'; // you can use <a> as well
		}


		if(isset($_POST['dasi_is_ajax_reload']) ) {
			return json_encode(array(
				'data' => $dasi_event_output,
				'maxpages' => $dasi_events->max_num_pages
			));
		}

		return $dasi_event_output;
	}else{

		if(isset($_POST['dasi_is_ajax_reload']) ) {
			return json_encode(array(
				'data' => '<div class="dasi_noevents_message">Sorry, no events found. Please try altering your search.</div>',
				'maxpages' => ''
			));
		}

		return false;
	}

}
add_shortcode( 'DasiEventFilter', 'DasiEventFilter' );



function dasi_event_filter($location = '') {

	$dasi_event_output = '';
	$dasi_event_output = $dasi_event_output . '
	<section class="event-filters"><div class="row column event-filters--inner"><form class="event-filters--form">';

	//list dates (11months forward)

	$dasi_event_output = $dasi_event_output . '<label class="event-filters--col">Date
		<input name="dasi-date" class="event-filters--input" type="text" id="dasi-filter-date"/>
		<input name="dasi-date-start"  style="display:none;" type="text" id="dasi-filter-date-start"/>
		<input name="dasi-date-end" class="hidden" style="display:none;" type="text" id="dasi-filter-date-end"/>
		</label>';


	//list venues - how??
	$dasi_event_output = $dasi_event_output . '<label style="display:none;" aria-hidden class="event-filters--col">Theme<select class="event-filters--select" id="dasi-filter-theme">';
	$themes = get_meta_values('dasi_event_theme','dasi_event',$location);
	$dasi_event_output = $dasi_event_output . '<option value="all-themes">All Themes</option>';
	foreach($themes as $theme) {
		$dasi_event_output = $dasi_event_output . '<option value="'.$theme.'">'.$theme.'</option>';
	}
	$dasi_event_output = $dasi_event_output . '</select></label>';

	//list genres - how??
	$genre_options = "";
	$dasi_event_output = $dasi_event_output . '<label class="event-filters--col">Genre<select class="event-filters--select" id="dasi-filter-genre">';
	$genres = get_meta_values('dasi_event_genre','dasi_event',$location);
	$dasi_event_output = $dasi_event_output . '<option value="all-genres">All Genres</option>';
	foreach($genres as $genre) {
		$dasi_event_output = $dasi_event_output . '<option value="'.$genre.'">'.$genre.'</option>';
	}

	$dasi_event_output = $dasi_event_output . '</select></label>';
	
	$dasi_event_output = $dasi_event_output . '<input type="submit" id="dasi-filter" value="Find Events" /></form></div></section>';

	wp_reset_query();
	return $dasi_event_output;
}



function get_meta_values( $key = '', $type = 'post', $location = '', $status = 'publish' ) {

    global $wpdb;

    if( empty( $key ) )
        return;

    //get location posts first
    if(!empty($location)) {
		$meta_args = array(
			'post_type'	=> 'dasi_event',
			'posts_per_page' => -1,
			'post_status' => 'publish',
		   	'meta_query' => array(
		       array(
		           'key' => 'dasi_event_location',
		           'value' => $location,
		           'compare' => '=',
		       )
		   )
		);
		$metquery = new WP_Query($meta_args);	
		$postidarr = [];
		if($metquery->have_posts()) {
			while($metquery->have_posts()) {
				$metquery->the_post();
				array_push($postidarr, get_the_ID() );
			}
		}

		wp_reset_query();
 
	    $r = $wpdb->get_col( $wpdb->prepare( "
	        SELECT pm.meta_value FROM {$wpdb->postmeta} pm
	        LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id
	        WHERE pm.meta_key = '%s' 
	        AND p.post_status = '%s' 
	        AND p.post_type = '%s' 
	        AND p.ID IN (".implode(",",$postidarr).")
	    ", $key, $status, $type ) );

	} else {

		$r = $wpdb->get_col( $wpdb->prepare( "
			SELECT pm.meta_value FROM {$wpdb->postmeta} pm
			LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id
			WHERE pm.meta_key = '%s' 
			AND p.post_status = '%s' 
			AND p.post_type = '%s'
		", $key, $status, $type ) );
	};

    $values = [];
    foreach($r as $v) {
      if(!is_null($v)) { array_push($values,$v); }
    }
    return array_unique($values);
}