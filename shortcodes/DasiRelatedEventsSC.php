<?php
//[DasiFilter location="" theme="" venue="" genre="" theme="" pagenum=(int)]
function DasiRelatedEventsFilter( $atts, $post = "" ){

	wp_reset_query();
	$a = shortcode_atts(array(
		'location'			=> '',
		'theme'				=> '',
		'genre'				=> '',
		'start_date'		=> '',
		'end_date'			=> '',
		'pagenum'			=> 1,
		'posts_per_page'	=> 3,
		'show_filter'		=> true
		), $atts);

	$dasi_event_args = array(
		'post_type'			=> 'dasi_event',
		'post_status'		=> 'publish',
		'posts_per_page'	=> $a['posts_per_page'],
		'paged'				=> $a['pagenum'],
		'order'				=> 'ASC',
		'orderby' 			=> 'meta_value',
		'meta_key' 			=> 'dasi_event_first_date'
		
	);



	//see if the post is an event
	if ( get_post_type( get_the_ID() ) == 'dasi_event' ) {
	    $dasi_is_event = true;
	}else{
		$dasi_is_event = false;
	}

	if($dasi_is_event) {
		//get the location
		$a['location'] = get_post_meta(get_the_ID(), 'dasi_event_location',true);
		
		//get the genre
		$a['genre'] = get_post_meta(get_the_ID(), 'dasi_event_genre',true);

		//get the theme
		$a['theme'] = get_post_meta(get_the_ID(), 'dasi_event_theme',true);

	}

	//only include IF required
	$dasi_event_args['meta_query']	= array(
		'relation'    => 'AND',
	);

	//LOCATION
	if(!empty($a['location'])) {
		$dasi_event_args['meta_query']['event_location'] =
			[
				"key" 		=> "dasi_event_location",	
				"value" 	=> $a['location'],
				"compare" 	=> "=",
			];
	}

	//THEME FILTER
	if(!empty($a['theme'])) {
		$dasi_event_args['meta_query']['event_theme'] =
			[
				"key" 		=> "dasi_event_theme",	
				"value" 	=> $a['theme'],
				"compare" 	=> "=",
			];
	}

	//GENRE
	if(!empty($a['genre'])) {
		$dasi_event_args['meta_query']['event_genre'] = 
			[
				"key" 		=> "dasi_event_genre",	
				"value" 	=> $a['genre'],
				"compare" 	=> "=",
			];

	}


	$dasi_event_args['meta_query']['last_date'] = 
		[
			"key" 		=> "dasi_event_last_date",	
			"value" 	=> date("Y-m-d"),
			"compare" 	=> ">=",
			"type"	=> "DATE",
			
		];




	// echo '<pre>';
	// var_dump($dasi_event_args);
	// echo '</pre>';

	$dasi_events = new WP_Query($dasi_event_args);
	$dasi_event_output = '';

	if($dasi_events->have_posts()) {
		$dasi_event_output = $dasi_event_output . '<div class="row event-listing--row dasi-related-events">';


		while($dasi_events->have_posts()) {
			$dasi_events->the_post();


			$dasi_event_location  	= get_post_meta(get_the_ID(), 'dasi_event_location',true);
			$dasi_event_genre  		= get_post_meta(get_the_ID(), 'dasi_event_genre',true);
			$dasi_event_first_date 	= date('d-M-Y',strtotime(get_post_meta(get_the_ID(), 'dasi_event_first_date',true)));
			$dasi_event_last_date 	= date('d-M-Y',strtotime(get_post_meta(get_the_ID(), 'dasi_event_last_date',true)));
			$final_date				= $dasi_event_first_date .' - ' . $dasi_event_last_date;
			if($dasi_event_first_date == $dasi_event_last_date) {
				$final_date = $dasi_event_first_date;
			}

			//get the correct image, and fallback if not found.
			$dasi_event_image = get_the_post_thumbnail_url('featured-small');
			if(empty($dasi_image)) {
				$dasi_event_image = get_the_post_thumbnail_url();
			}

			$dasi_event_output = $dasi_event_output . '<div class="small-12 large-4 column">
		        <div class="card '.sanitize_title($dasi_event_location).' " data-location="'.sanitize_title($dasi_event_location).'">
		        	<div class="card--image" style="background-image: url('.$dasi_event_image .')"></div>
		            <div class="card--desc">
		                <h4 class="card--title">'.get_the_title().'</h4>
		                <p>
		                    ' . $final_date . '
		                    <br />
		                    ' . $dasi_event_location . '
		                </p>
		                <a class="primary-btn card--btn" href="' . get_permalink() . '">Book now</a>
		            </div>
		        </div><!-- // card -->
		    </div>';
		}//endwhile

		$dasi_max_pages = $dasi_events->max_num_pages;

		$dasi_event_output = $dasi_event_output . '</div><!-- // row -->';


		if(isset($_POST['dasi_is_ajax_reload']) ) {
			return json_encode(array(
				'data' => $dasi_event_output,
				'maxpages' => $dasi_events->max_num_pages
			));
		}

		return $dasi_event_output;
	}else{
		return false;
	}


}
add_shortcode( 'DasiRelatedEventsFilter', 'DasiRelatedEventsFilter' );