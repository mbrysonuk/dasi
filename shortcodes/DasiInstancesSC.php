<?php
//[DasiInstanceFilter eventid=(int)]
function DasiInstanceFilter( $atts, $post = "" ){
	$a = shortcode_atts(array(
		'eventid'			=> 0,
		'start_date'		=> '',
		'pagenum'			=> 1,
		'posts_per_page'	=> 12,
		'show_filter'		=> true

		), $atts);

	$a['eventid'] = get_post_meta(get_the_ID(), 'dasi_event_id',true);


	$dasi_onsaleweb = get_post_meta(get_the_ID(), 'dasi_onsaleweb',true);


	if($dasi_onsaleweb == "false" ) {
		return;
	}

	if($post == "" && empty($a['eventid']) ) {
		return false;
	}

	if($post != "") {
		$a['pagenum'] 			= $post['dasi_pagenum'];
		$a['start_date']		= $post['dasi_startdate'];
		$a['end_date']			= $post['dasi_enddate'];
		$a['pagenum']			= $post['dasi_pagenum'];
		$a['posts_per_page']	= $post['dasi_postsperpage'];
	}

	if(isset($_POST['dasi_event_id'])) {
		$a['eventid'] = $_POST['dasi_event_id'];
	}

		// if(isset($_GET['eventid'])) {
		// 	$a['eventid'] = $_GET['eventid'];
		// }else if( !empty(get_post_meta(get_the_ID(), 'dasi_event_id',true)) ) {
			
		// };

		//what is this?
		// show a list of instances, pagination with ajax, on event detail pages.
		//assumes GET paramater will be used like so ?evendid=4001

	$dasi_ins_args = array(
		'post_type'			=> 'dasi_instance',
		'post_status'		=> 'publish',
		'meta_key'			=> 'dasi_related_event_id',
		'meta_value'		=> $a['eventid'],
		'posts_per_page'	=> $a['posts_per_page'],
		'paged'				=> $a['pagenum'],

	    'orderby' => array(
	        'dasi_instance_start_date' => 'ASC',
	        'dasi_instance_start_time' => 'ASC',
	    ),
	);
	

	//only include IF required
	$dasi_ins_args['meta_query']	= array(
		'relation'    => 'AND',
	);

	//only show events that are in the future.
	$dasi_ins_args['meta_query']['dasi_instance_start_date'] = 
		[
			"key" 		=> "dasi_instance_start_date",	
			"value" 	=> date("Y-m-d"),
			"compare" 	=> ">=",
			"type"	=> "DATE",
			
		];


	$dasi_ins_args['meta_query']['dasi_instance_start_time'] =
		[
			"key" 		=> "dasi_instance_start_time",
			"compare" 	=> "EXISTS",
			"type"	=> "DATE",
			
		];

	
	//var_dump($dasi_onsaleweb);
	// echo '<pre>';
	// var_dump($dasi_ins_args);
	// echo '</pre>';


	$dasi_instance_query 	= new WP_Query($dasi_ins_args);

	$instance_response 	= '';
	if($dasi_instance_query->have_posts()) {
		if($a['pagenum'] == 1) {
		$instance_response = $instance_response . '<ul
			class="dasi_instance_loop"
			id="dasi-instances"
			dasa-event-id="'.$a['eventid'].'"
			data-postsperpage="'.$a['posts_per_page'].'"
			data-pagenum="'.$a['pagenum'].'"
			data-pagesmax="'.$dasi_instance_query->max_num_pages.'">';
		}

		while($dasi_instance_query->have_posts()) {
			$dasi_instance_query->the_post();
			//get availability status
			$dasi_instance_seats = get_post_meta(get_the_ID(), 'dasi_instance_seatsavailable',true);
			$dasi_instance_capacity = get_post_meta(get_the_ID(), 'dasi_instance_capacity',true);
			
			$dasi_instance_availability = '';
			$dasi_instance_availability_text = '';
			//capacity - seats available = seats taken
			//
			//

			//seats available / capacity = %availability
			if(($dasi_instance_seats) == 0) {
				$dasi_instance_availability = 'sold-out';
				$dasi_instance_availability_text = 'Sold Out';
			}else if(($dasi_instance_seats / $dasi_instance_capacity) <= 0.1) {
				$dasi_instance_availability = 'low-availability';
				$dasi_instance_availability_text = 'Low Availability';
			}



			$instance_response = $instance_response . '<li class="instance-card '.$dasi_instance_availability.'">';
				$instance_response = $instance_response . '<div class="dasi_info">';
					$instance_response = $instance_response . '<span class="dasi_title">'. get_the_title() . '</span>';

					$dasi_clean_date = explode("-",get_post_meta(get_the_ID(),'dasi_instance_start_date','dasi_instance'));
					$dasi_clean_time = explode(":",get_post_meta(get_the_ID(),'dasi_instance_start_time','dasi_instance'));

					$instance_response = $instance_response . '<span class="dasi_dates">'. $dasi_clean_date[2] . "-" . $dasi_clean_date[1] . "-" . $dasi_clean_date[0] . '</span>';
					$instance_response = $instance_response . '<span class="dasi_times">'. $dasi_clean_time[0] . ":" . $dasi_clean_time[1] . '</span>';
					$instance_response = $instance_response . '<span class="dasi_accessicons">';

						$access_icon_val = get_post_meta(get_the_ID(),'dasi_wheelchairaccess','dasi_instance');
						if($access_icon_val == "1") {
							$instance_response = $instance_response . '<span class="dasi_wheelchairaccess">WHEEL</span>';
						}
						$access_icon_val = get_post_meta(get_the_ID(),'dasi_captioned','dasi_instance');
						if($access_icon_val == "1") {
							$instance_response = $instance_response . '<span class="dasi_captioned">CAP</span>';
						}

						$access_icon_val = get_post_meta(get_the_ID(),'dasi_bslinterpreted','dasi_instance');
						if($access_icon_val == "1") {
							$instance_response = $instance_response . '<span class="dasi_bslinterpreted">BSL</span>';
						}

						$access_icon_val = get_post_meta(get_the_ID(),'dasi_audiodescription','dasi_instance');
						if($access_icon_val == "1") {
							$instance_response = $instance_response . '<span class="dasi_audiodescription">AUDIODESC</span>';
						}

				 	$instance_response = $instance_response . '</span><span class="availability '.$dasi_instance_availability.'">'.$dasi_instance_availability_text.'</span>';

				$instance_response = $instance_response . '</div>';

				if($dasi_onsaleweb != "false") {
					if($dasi_instance_availability == 'sold-out') {
						$instance_response = $instance_response . '<div class="dasi_book primary-btn">SOLD OUT</div>';
					}else{
						$instance_response = $instance_response . '<div class="dasi_book"><a href="/book/?instanceid='.get_post_meta(get_the_ID(),'dasi_instance_id','dasi_instance').'">BOOK NOW</a></div>';
					}
				}
				
			$instance_response = $instance_response . '</li>';
		}
		if($a['pagenum'] == 1) {
			$instance_response = $instance_response . '</ul>';

			if($dasi_instance_query->max_num_pages > 1) {
				$instance_response = $instance_response . '<div class="row column align-center dasi-load-more-parent text-center"><div class="primary-btn dasi_i_loadmore text-center">LOAD MORE</div></div>';
			}
		}

	}

	return $instance_response;

}
add_shortcode( 'DasiInstanceFilter', 'DasiInstanceFilter' );