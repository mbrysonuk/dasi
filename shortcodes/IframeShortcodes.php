<?php

//[DasiEventList]
function DasiEventList( $atts ){
	$path = '/website/eventlist.aspx?resize=true';
	return Dasi\DasiFrame::ConstructIframe($path);
}
add_shortcode( 'DasiEventList', 'DasiEventList' );

//[DasiEventList]
function DasiBasket( $atts ){
	$path = '/website/basket.aspx?resize=true';
	return Dasi\DasiFrame::ConstructIframe($path);
}
add_shortcode( 'DasiBasket', 'DasiBasket' );

//[DasiEventDetails]
function DasiEventDetails( $atts ){
	$path = '/website/EventDetails.aspx?resize=true';
	return Dasi\DasiFrame::ConstructIframe($path);
}
add_shortcode( 'DasiEventDetails', 'DasiEventDetails' );

//choose seats
//[DasiChooseSeats instanceid="(int)" ]
//https://system.spektrix.com/sunderlandculture_staging/website/ChooseSeats.aspx?EventInstanceId=9604
function DasiChooseSeats( $atts ){

	$a = shortcode_atts(array(
		'instanceid'			=> 0,
		), $atts);

		if(isset($_GET['instanceid'])) {
			$a['instanceid'] = $_GET['instanceid'];
		}else if( !empty(get_post_meta(get_the_ID(), 'dasi_event_id',true)) ) {
			$a['instanceid'] = get_post_meta(get_the_ID(), 'dasi_event_id',true);
		};


	$path = '/website/ChooseSeats.aspx?resize=true&EventInstanceId='.$a['instanceid'];
	return Dasi\DasiFrame::ConstructIframe($path);
}
add_shortcode( 'DasiChooseSeats', 'DasiChooseSeats' );



//[DasiMyAccount]
function DasiMyAccount( $atts ){
	$path = '/website/secure/myaccount.aspx?resize=true';
	return Dasi\DasiFrame::ConstructIframe($path);
}
add_shortcode( 'DasiMyAccount', 'DasiMyAccount' );

//[DasiCheckout]
function DasiCheckout( $atts ){
	$path = '/website/secure/checkout.aspx?resize=true';
	return Dasi\DasiFrame::ConstructIframe($path);
}
add_shortcode( 'DasiCheckout', 'DasiCheckout' );

//[DasiDonations]
function DasiDonations( $atts ){
	$path = '/website/Donations.aspx?resize=true';
	return Dasi\DasiFrame::ConstructIframe($path);
}
add_shortcode( 'DasiDonations', 'DasiDonations' );

//[DasiCheckout]
function DasiHeaderWidgets(){
	$dasi_clientname = get_option('dasi_spektrix_clientname');
	return '<script src="https://system.spektrix.com/'.$dasi_clientname.'/api/v3/webcomponents/js"></script><link rel="import" href="https://system.spektrix.com/'.$dasi_clientname.'/api/v3/webcomponents/html" />'
	.'<spektrix-basket-item-count></spektrix-basket-item-count>'
	.'<spektrix-basket-total></spektrix-basket-total>'
	.'<spektrix-basket-discount-total></spektrix-basket-discount-total>'
	;
}
add_shortcode( 'DasiHeaderWidgets', 'DasiHeaderWidgets' );


//[DasiEventAttributes]
function DasiEventAttributes( $atts ){


// 	$response =
// 	'<ul class="dasi-event-attributes">' . 
// 		'<li><a href=""</li>' .
// 		'<li></li>' .
// location
// dates
// price range
// Duration
// Theatre
// age range
// event description





// if(!empty()) {

// }

$dasi_attr_dates 		= get_post_meta(get_the_ID(), 'dasi_event_dates',true);

$dasi_attr_duration 	= get_post_meta(get_the_ID(), 'dasi_event_duration',true);
$dasi_attr_pricerange 	= get_post_meta(get_the_ID(), 'dasi_event_pricerange',true);
$dasi_attr_agerange 	= get_post_meta(get_the_ID(), 'dasi_event_age_range',true);

$dasi_attr_location 	= get_post_meta(get_the_ID(), 'dasi_event_location',true);
$dasi_attr_theme	 	= get_post_meta(get_the_ID(), 'dasi_event_theme',true);
$dasi_attr_genre 	 	= get_post_meta(get_the_ID(), 'dasi_event_genre',true);


echo '<ul>';

//Dates
if(!empty($dasi_attr_dates)) {
	echo '<li><span class="dasi-attr-title">Dates: </span><span class="dasi-attr-details">'.$dasi_attr_dates.'</span></li>';
}

//Location
if(!empty($dasi_attr_location)) {
	echo '<li><span class="dasi-attr-title">Location: </span><span class="dasi-attr-details">'.$dasi_attr_location.'</span></li>';
}

//Duration
if(!empty($dasi_attr_duration)) {
	echo '<li><span class="dasi-attr-title">Duration: </span><span class="dasi-attr-details">'.$dasi_attr_duration.' (minutes)</span></li>';
}

//Price Range
if(!empty($dasi_attr_pricerange)) {
	echo '<li><span class="dasi-attr-title">Price Range: </span><span class="dasi-attr-details">'.$dasi_attr_pricerange.'</span></li>';
}

//age range
if(!empty($dasi_attr_agerange)) {
	echo '<li><span class="dasi-attr-title">Age Range: </span><span class="dasi-attr-details">'.$dasi_attr_agerange.'</span></li>';
}


//Theme
if(!empty($dasi_attr_theme)) {
	echo '<li><span class="dasi-attr-title">Theme: </span><span class="dasi-attr-details">'.$dasi_attr_theme.'</span></li>';
}


//Genre
if(!empty($dasi_attr_genre)) {
	echo '<li><span class="dasi-attr-title">Genre: </span><span class="dasi-attr-details">'.$dasi_attr_genre.'</span></li>';
}

echo '</ul>';

}
add_shortcode( 'DasiEventAttributes', 'DasiEventAttributes' );