<?php

/*
	Plugin Name: DASI
	Plugin URI: https://mbryson.co.uk/plugins/dasi/
	description: Digital Allies Spektrix Integration Plugin. v1.1
	Version: 1.1
	Author: Matthew Bryson
	Author URI: https://digitalallies.co.uk
	License: GPL2
*/

/*--
*	Variables
*	@todo replace with options
--*/
// $api_key = '0d9c6d14-e336-4b7e-8513-d8dfc3851b97';
// $ssl_cert_path = '/Users/matthewbryson/scult/sunderlandculture-digitalallies-2018.crt';
// $ssl_key_path = '/Users/matthewbryson/scult/sunderlandculture.key';
add_option('dasi_spektrix_clientname','');
add_option('dasi_spektrix_apikey','');
//add_option('dasi_spektrix_crtpath','/Users/matthewbryson/scult/sunderlandculture-digitalallies-2018.crt');
//add_option('dasi_spektrix_keypath','/Users/matthewbryson/scult/sunderlandculture.key');
add_option('dasi_spektrix_crtpath','');
add_option('dasi_spektrix_keypath','');

/*--
*
*	Controllers
*	
--*/
require_once('controllers/Dasi.php');

/*--
*
*	Models
*	
--*/
require_once('models/DasiEvent.php');
require_once('models/DasiFrame.php');
require_once('models/DasiInstance.php');
require_once('functions/DasiCronJobs.php');
require_once('functions/DasiSignUp.php');


/*--
*
*	Shortcodes
*	
--*/
require_once('shortcodes/IframeShortcodes.php');
require_once('shortcodes/DasiEventFilterSC.php');
require_once('shortcodes/DasiRelatedEventsSC.php');
require_once('shortcodes/DasiInstancesSC.php');

/*--
*	Custom Post Types
--*/
require_once('functions/event_cpt.php');
require_once('functions/instance_cpt.php');

/*--
*	Templates
--*/

require_once('views/admin/index.php');


/*--
* Admin table
--*/

// handled by a third party plugin now


/*--
*	Hooks
--*/
register_activation_hook(__FILE__,array('Dasi\Dasi','dasi_activate'));
//register_activation_hook(__FILE__, 'dasi_imports_event');
register_deactivation_hook(__FILE__, array( 'Dasi\Dasi', 'dasi_deactivate' ) );
register_uninstall_hook(__FILE__, array( 'Dasi\Dasi', 'dasi_uninstall' ) );

//inject into head on every load, required for iframes
add_action('wp_head', array('Dasi\DasiFrame', 'SetIframeHead' ));


//AJAX STUFF
add_action( 'wp_enqueue_scripts', 'dasi_enqueue_scripts' );
function dasi_enqueue_scripts(){
  wp_enqueue_script( 'dasi_jquery', 'https://cdn.jsdelivr.net/jquery/latest/jquery.min.js');
  wp_enqueue_script( 'dasi_jquerymoment', 'https://cdn.jsdelivr.net/momentjs/latest/moment.min.js');
  wp_enqueue_script( 'dasi_daterangepicker', 'https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js');
  wp_enqueue_style( 'dasi_daterangepickercss', 'https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css' );
  wp_register_script( 'dasi_ajaxHandle', plugins_url('scripts/dasi_ajax.js', __FILE__), array(), false, true );
  wp_enqueue_script( 'dasi_ajaxHandle' );
  wp_localize_script( 'dasi_ajaxHandle', 'ajax_object', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
}


add_action( "wp_ajax_dasiloadmore", "dasi_wp_ajax_eventloadmore_function" );
add_action( "wp_ajax_nopriv_dasiloadmore", "dasi_wp_ajax_eventloadmore_function" );
function dasi_wp_ajax_eventloadmore_function(){
  //DO whatever you want with data posted
  //To send back a response you have to echo the result!
  //echo 'mbtest:' . $_POST['dasi_pagenum'] . "<br>";
  echo DasiEventFilter( [], $_POST );
  wp_die(); // ajax call must die to avoid trailing 0 in your response
}

add_action( "wp_ajax_dasiinstanceloadmore", "dasi_wp_ajax_instanceloadmore_function" );
add_action( "wp_ajax_nopriv_dasiinstanceloadmore", "dasi_wp_ajax_instanceloadmore_function" );
function dasi_wp_ajax_instanceloadmore_function(){
  //DO whatever you want with data posted
  //To send back a response you have to echo the result!
  //echo 'mbtest:' . $_POST['dasi_pagenum'] . "<br>";
  echo DasiInstanceFilter( $atts, $_POST );
  wp_die(); // ajax call must die to avoid trailing 0 in your response
}