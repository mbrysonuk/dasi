DASI (Digital Allies Spektrix Integration)

A plugin for WorPress to handle the Spektrix integration using a mix of API v1 and Iframes.


How to use:

You have a few integration options. the first is more basic and relies solely on the iframes. The second is slightly more complex and requires some additions to templates.


Option 1 - Basic
1. Go to DASI > Options
2. Add you spektrix client name in the field provided.
3. Ensure you also input your Spektrix API, as well as your paths to your CRT and KEY files. Speak to your develop if you are unsure.
4. create the following pages, and add the following shortcodes to them in the WYSIWYG editor.
	- Events List - [DasiEventList]
	- Checkout - [DasiCheckout]
	- My Account - [DasiMyAccount]
5. navigate to the page, you will see the iframes and will be able to complete a checkout!
6. Speak to your Spektrix consultant for styling these pages.


Option 2 - More complicated
1. Go to DASI > Options
2. Add you spektrix client name in the field provided.
3. Ensure you also input your Spektrix API, as well as your paths to your CRT and KEY files. Speak to your develop if you are unsure.
4. Create a template for handling dasi_events custom post type (filename can be dasi_events-single.php). Add the function below where you want the instances to appear;
	<?php
		
	?>
5. create the following pages, and add the following shortcodes to them in the WYSIWYG editor.
	- Event - [DasiEventFilter] {multiple options available - @todo add these to readme}
	- Choose Seats (handle must be /book/)- [DasiChooseSeats]
	- Checkout - [DasiCheckout]
	- My Account - [DasiMyAccount]
6. navigate to the page, you will see the iframes and will be able to complete a checkout!
7. Speak to your Spektrix consultant for styling these pages.





echo do_shortcode('[DasiInstanceFilter]');


--SHORTCODES
[DasiEventList] - lists events (not used)
[DasiBasket] - the overall basket page (not used)
[DasiEventDetails] - single event detail page (not used)
[DasiChooseSeats] - choose seats page. the page where users go once an event is selected.
[DasiMyAccount] - place to log in
[DasiCheckout] - checkout
[DasiDonations] - donations
[DasiHeaderWidgets] - needs to be shown on every header
[DasiEventFilter] - show event listing and filter with all fucntionality
[DasiInstanceFilter] - show list of instances for event (on event page only)

