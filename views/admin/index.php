<?php


add_action('admin_menu', 'DasiMenu');
 
function DasiMenu(){
        add_menu_page( 'Dasi Plugin Page', 'DASI', 'manage_options', 'dasi', 'dasi_options_page' );
        
}



// settings

function dasi_register_settings() {

   add_option( 'dasi_spektrix_apikey', 		get_option( 'dasi_spektrix_apikey' ));
   add_option( 'dasi_spektrix_clientname', 	get_option( 'dasi_spektrix_clientname' ));
   add_option( 'dasi_spektrix_crtpath', 	get_option( 'dasi_spektrix_crtpath' ));
   add_option( 'dasi_spektrix_keypath', 	get_option( 'dasi_spektrix_keypath' ));

   register_setting( 'dasi_options_group', 'dasi_spektrix_apikey', 'dasi_callback' );
   register_setting( 'dasi_options_group', 'dasi_spektrix_clientname', 'dasi_callback' );
   register_setting( 'dasi_options_group', 'dasi_spektrix_crtpath', 'dasi_callback' );
   register_setting( 'dasi_options_group', 'dasi_spektrix_keypath', 'dasi_callback' );

}
add_action( 'admin_init', 'dasi_register_settings' );




function dasi_register_options_page() {
  add_options_page('Page Title', 'Plugin Menu', 'manage_options', 'dasi', 'dasi_options_page');
}
add_action('admin_menu', 'dasi_register_options_page');



function dasi_options_page()
{
?>

	<h1>DASI</h1>
	<p>Digital Allies Spektrix Integration</p>
	<h2>How to use this plugin</h2>
	<p>Add the following shortcodes to your site and update your Spektrix ingration to point to the pages you set up with these shortcodes in them!</p>

	<p>[DasiEventList] - lists events (not used)</p>
	<p>[DasiBasket] - the overall basket page (not used)</p>
	<p>[DasiEventDetails] - single event detail page (not used)</p>
	<p>[DasiChooseSeats] - choose seats page. the page where users go once an event is selected.</p>
	<p>[DasiMyAccount] - place to log in</p>
	<p>[DasiCheckout] - checkout</p>
	<p>[DasiDonations] - donations</p>
	<p>[DasiHeaderWidgets] - needs to be shown on every header</p>
	<p>[DasiEventFilter] - show event listing and filter with all fucntionality</p>
	<p>[DasiInstanceFilter] - show list of instances for event (on event page only)</p>
	<hr>
	<h2>Integration Settings</h2>

	<div>
		
		<form method="post" action="options.php">
			<?php settings_fields( 'dasi_options_group' ); ?>
			    <table class="form-table">
				    <tr valign="top">
				      	<th scope="row">APIKEY:</th>
				      	<td><input style="width: 100%;" type="text" name="dasi_spektrix_apikey" value="<?php echo get_option( 'dasi_spektrix_apikey' ); ?>"/></td>
				  	</tr>
				  	<tr valign="top">
				  		<th scope="row">CLIENTNAME:</th>
				    	<td><input style="width: 100%;" type="text" name="dasi_spektrix_clientname" value="<?php echo get_option( 'dasi_spektrix_clientname' ); ?>"/></td>
				    </tr>

				  	<tr valign="top">
				  		<th scope="row">CRT PATH:</th>
				    	<td><input style="width: 100%;" type="text" name="dasi_spektrix_crtpath" value="<?php echo get_option( 'dasi_spektrix_crtpath' ); ?>"/></td>
				    </tr>

				  	<tr valign="top">
				  		<th scope="row">KEY PATH:</th>
				    	<td><input style="width: 100%;" type="text" name="dasi_spektrix_keypath" value="<?php echo get_option( 'dasi_spektrix_keypath' ); ?>"/></td>
				    </tr>

			    </table>


			<?php  submit_button(); ?>
		</form>
	</div>


    <hr>
    <h1>Manual Import</h1>
    <p>The API pulls through once every 60 minutes, updating the events and instances on the site.</p>
    <p>The only data not updated is the title and description.</p>
    <p>Normally, you can just leave this be, it works for you! However, if you need to then you can manually update events and instances below;</p>
	<form action="" method="POST">
		<input name="run_update" type="text" class="hide" value="run_update"/>
		<input type="submit" class="button" value="Run Update!"/>
	</form>

	
	<?php if(isset($_POST['run_update'])) { ?>
		<hr>
		<h2>Progress:</h2>

		<?php
			$dasi_events = new Dasi\DasiEvent;
			$dasi_events->import();
			$message = "Complete.";
		?>


	<?php } ?>	
<?php
}