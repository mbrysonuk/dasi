<?php 
	$dasi_event_id 			= get_post_meta( $post->ID, 'dasi_event_id', true );//event @attributes id
	$dasi_event_price_range	= get_post_meta( $post->ID, 'dasi_event_price_range', true );//event price range
	$dasi_event_genre 		= get_post_meta( $post->ID, 'dasi_event_genre', true );//event genre
	$dasi_event_theme 		= get_post_meta( $post->ID, 'dasi_event_theme', true );//event theme
	$dasi_event_location 	= get_post_meta( $post->ID, 'dasi_event_location', true );//event location
	$dasi_event_theatre		= get_post_meta( $post->ID, 'dasi_event_theatre', true );//event theatre company
	$dasi_event_age_range	= get_post_meta( $post->ID, 'dasi_event_age_range', true );//event age range

	//event image
	//event thumb image
	//

?>
<div class="fieldset dasi_fieldset">

	<label for="dasi_event_id">Event ID</label><br>
	<span>You can't edit this. Its used to sync this record with Spektrix, as well as bookings. For information purposes only.</span><br>
	<input name="dasi_event_id" class="widefat" value="<?php echo esc_textarea( $dasi_event_id ); ?>"><br>

	<label for="dasi_event_pricing">Event Price Range</label><br>
	<input type="text" name="dasi_event_pricing" value="<?php echo esc_textarea( $dasi_event_price_range ); ?>" class="widefat"><br>

	<label for="dasi_event_genre">Event Genre</label><br>
	<input type="text" name="dasi_event_genre" value="<?php echo esc_textarea( $dasi_event_genre ); ?>" class="widefat"><br>

	<label for="dasi_event_theme">Event Theme</label><br>
	<input type="text" name="dasi_event_theme" value="<?php echo esc_textarea( $dasi_event_theme ); ?>" class="widefat"><br>

	<label for="dasi_event_location">Event Location</label><br>
	<input type="text" name="dasi_event_location" value="<?php echo esc_textarea( $dasi_event_location ); ?>" class="widefat"><br>

	<label for="dasi_event_theatre">Event Theatre</label><br>
	<input type="text" name="dasi_event_theatre" value="<?php echo esc_textarea( $dasi_event_theatre ); ?>" class="widefat"><br>

	<label for="dasi_event_age_range">Event Age Range</label><br>
	<input type="text" name="dasi_event_age_range" value="<?php echo esc_textarea( $dasi_event_age_range ); ?>" class="widefat"><br>

</div>