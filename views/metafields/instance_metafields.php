<?php 
	$dasi_instance_id 				= get_post_meta( $post->ID, 'dasi_instance_id', true );//instance @attributes id
	$dasi_event_id 					= get_post_meta( $post->ID, 'dasi_event_id', true );//instance @attributes id
	$dasi_instance_start_date 		= get_post_meta( $post->ID, 'dasi_instance_start_date', true );	
	$dasi_instance_end_date 		= get_post_meta( $post->ID, 'dasi_instance_end_date', true );	
	$dasi_instance_start_time 		= get_post_meta( $post->ID, 'dasi_instance_start_time', true );
	$dasi_instance_end_time 		= get_post_meta( $post->ID, 'dasi_instance_end_time', true );

	$dasi_instance_pricing			= get_post_meta( $post->ID, 'dasi_instance_pricing', true );//instance price range

?>
<div class="fieldset dasi_fieldset">

<label for="dasi_instance_id">Instance ID</label><br>
<span>You can't edit this. Its used to sync this record with Spektrix, as well as bookings. For information purposes only.</span><br>
<input name="dasi_instance_id" class="widefat" value="<?php echo esc_textarea( $dasi_instance_id ); ?>" disabled ><br>

<label for="dasi_instance_id">Event ID</label><br>
<span>Related Event ID. For information purposes only.</span><br>
<input name="dasi_event_id" class="widefat" value="<?php echo esc_textarea( $dasi_instance_id ); ?>" disabled ><br>

<label for="dasi_instance_start_date">instance Start Date</label><br>
<input type="date" name="dasi_instance_start_date" value="<?php echo esc_textarea( $dasi_instance_start_date ); ?>" class="widefat"><br>

<label for="dasi_instance_end_date">instance End Date</label><br>
<input type="date" name="dasi_instance_end_date" value="<?php echo esc_textarea( $dasi_instance_end_date ); ?>" class="widefat"><br>

<label for="dasi_instance_start_time">instance Start Time</label><br>
<input type="time" name="dasi_instance_start_time" value="<?php echo esc_textarea( $dasi_instance_start_time ); ?>" class="widefat"><br>

<label for="dasi_instance_end_time">instance End Time</label><br>
<input type="time" name="dasi_instance_end_time" value="<?php echo esc_textarea( $dasi_instance_end_time ); ?>" class="widefat"><br>

<label for="dasi_instance_pricing">instance Pricing</label><br>
<input type="text" name="dasi_instance_pricing" value="<?php echo esc_textarea( $dasi_instance_pricing ); ?>" class="widefat"><br>

</div>