<?php

function create_event_cpt() {
  register_post_type( 'dasi_event',
    array(
      'labels' => array(
        'name' => __( 'Events' ),
        'singular_name' => __( 'Event' )
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'events'),
      'supports' => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'page-attributes' ),
    )
  );
}
add_action( 'init', 'create_event_cpt' );

//custom meta

//event id
//event date (instance)
//event time (instance)
//event pricing
//event genre
//event theme
//event location
//event theatre
//event age range
//event access icons

//NOT ADDED YET
//event image
//thumb_image
//web_event_id
//attributes
//duration


//create the new meta box
function dasi_add_event_metaboxes() {
  add_meta_box(
    'dasi_event_options',//id
    'DASI Event Options',//title
    'dasi_events_options',//callback function
    'dasi_event',//screen
    'normal',//context normal or side
    'default'//priority on page order
  );
}
add_action( 'add_meta_boxes', 'dasi_add_event_metaboxes' );



/**
 * Output the HTML for the metabox.
 */
function dasi_events_options() {
  global $post;
  // Nonce field to validate form request came from current site
  wp_nonce_field( basename( __FILE__ ), 'event_fields' );

  // Output the field
  include dirname(__DIR__) . '/views/metafields/event_metafields.php';

}

/**
 * Save the metabox data
 */
function dasi_save_events_meta( $post_id, $post ) {
  // Return if the user doesn't have edit permissions.
  if ( ! current_user_can( 'edit_post', $post_id ) ) {
    return $post_id;
  }
  // Verify this came from the our screen and with proper authorization,
  // because save_post can be triggered at other times.
  if(isset($_POST['event_fields'])){
    if ( ! wp_verify_nonce( $_POST['event_fields'], basename(__FILE__) ) ) {
      return $post_id;
    }
  } else {
    return $post_id;
  }

  // Now that we're authenticated, time to save the data.
  // This sanitizes the data from the field and saves it into an array $events_meta.
  $events_meta['dasi_event_id']				= esc_textarea( $_POST['dasi_event_id'] );
  // $events_meta['dasi_event_date']			= esc_textarea( $_POST['dasi_event_date'] );
  // $events_meta['dasi_event_time']			= esc_textarea( $_POST['dasi_event_time'] );
  $events_meta['dasi_event_pricing']		= esc_textarea( $_POST['dasi_event_pricing'] );
  $events_meta['dasi_event_genre']			= esc_textarea( $_POST['dasi_event_genre'] );
  $events_meta['dasi_event_theme']			= esc_textarea( $_POST['dasi_event_theme'] );
  $events_meta['dasi_event_location']		= esc_textarea( $_POST['dasi_event_location'] );
  $events_meta['dasi_event_theatre']		= esc_textarea( $_POST['dasi_event_theatre'] );
  $events_meta['dasi_event_age_range']		= esc_textarea( $_POST['dasi_event_age_range'] );
  $events_meta['dasi_event_access_icons']	= esc_textarea( $_POST['dasi_event_access_icons'] );



  // Cycle through the $events_meta array.
  // Note, in this example we just have one item, but this is helpful if you have multiple.
  foreach ( $events_meta as $key => $value ) :
    // Don't store custom data twice
    if ( 'revision' === $post->post_type ) {
      return;
    }
    if ( get_post_meta( $post_id, $key, false ) ) {
      // If the custom field already has a value, update it.
      update_post_meta( $post_id, $key, $value );
    } else {
      // If the custom field doesn't have a value, add it.
      add_post_meta( $post_id, $key, $value);
    }
    if ( ! $value ) {
      // Delete the meta key if there's no value
      delete_post_meta( $post_id, $key );
    }
  endforeach;
}
add_action( 'save_post', 'dasi_save_events_meta', 1, 2 );