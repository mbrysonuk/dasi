
<?php
  // Specify SSL client certificate key and certificate file names / paths
  // These files should not be in your website directories and should be protected // so that no-one can read them.
  $api_key = "d6a9f43b-7d08-4896-b1c0-d31320c961ce";
  $dasi_clientname = "sunderlandculture";
  $ssl_key_path = "/Users/matthewbryson/Sites/da/scult/sunderlandculture.key";
  $ssl_cert_path = "/Users/matthewbryson/Sites/da/scult/sunderlandculture-digitalallies-2018.crt";
  // Make CURL verbose
  $curlVerbose = false;

  // Magic quotes breaks submitted XML
  if (get_magic_quotes_gpc()) {
    $process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    while (list($key, $val) = each($process)) {
      foreach ($val as $k => $v) {
        unset($process[$key][$k]);
          if (is_array($v)) {
            $process[$key][stripslashes($k)] = $v;
            $process[] = &$process[$key][stripslashes($k)];
          } else {
            $process[$key][stripslashes($k)] = stripslashes($v);
          }//endif
          unset($process);
      }//endif
    }//endwhile
  }//endif

?>


<?php
if(isset($_POST['submit'])) {
  if($_POST['submit'] == true) {
  // Check we've got the parameters we require

  // Build the base URL we will be sending our request to
  $url = "https://api.system.spektrix.com/".$dasi_clientname."/api/v2/customers?api_key=" . $api_key;

  // Send the request via CURL
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_SSLCERT, $ssl_cert_path);
  curl_setopt($ch, CURLOPT_SSLKEY, $ssl_key_path);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_VERBOSE, $curlVerbose);
  curl_setopt($ch, CURLOPT_STDERR, fopen('php://output', 'w'));

  // CURLs handling of headers is less than idea
  $headers = "";
  
  function handleHeader($curlObj, $headerLine) {
    global $headers;
    $headers .= $headerLine;
    return strlen($headerLine);
  }
  curl_setopt($ch, CURLOPT_HEADERFUNCTION, 'handleHeader');
  curl_setopt($ch, CURLOPT_POST, true);
  
  $requestHeaders = array();
  // Send the body if we're doing a POST or PATCH
 
  $requestHeaders[] = "Content-Type: application/xml";
  curl_setopt($ch, CURLOPT_POSTFIELDS, $_POST['reqbody']);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $requestHeaders);
  
  // Perform the request

  $dasi_result = curl_exec($ch);

  $dasi_response = '';
  $dasi_response_status = "failure";
  $curlInfo = curl_getinfo($ch);
  if ($curlInfo['http_code'] == 0) {
    $dasi_response =  "CURL returned http_code 0 - have you correctly specified the client certificate?";
  }

  if ($curlInfo['http_code'] == 200) {
    $dasi_response_status = "success";
    $dasi_response =  'Success! You have been added to our mailing list.';
  }else{
    //print $headers;
    $dasi_response =  'There was an issue with your submission;'. $dasi_result .'';
  }

  }
}

echo '<script type="text/javascript">
           window.location = "https://sundercult.test/sign-up-test/?response='.urlencode($dasi_response_status).'&message='.urlencode($dasi_response).'";
      </script>';

?>

