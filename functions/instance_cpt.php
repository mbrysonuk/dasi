<?php

function create_instance_cpt() {
  register_post_type( 'dasi_instance',
	array(
	  'labels' => array(
		'name' => __( 'Event Instances' ),
		'singular_name' => __( 'instance Item' )
	  ),
	  'public' => true,
	  'has_archive' => true,
	  'rewrite' => array('slug' => 'event_items'),
	  'supports' => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'page-attributes' ),
	)
  );
}
add_action( 'init', 'create_instance_cpt' );


//create the new meta box
function dasi_add_instance_metaboxes() {
  add_meta_box(
	'dasi_instance_options',//id
	'DASI Instance Options',//title
	'dasi_instances_options',//callback function
	'dasi_instance',//screen
	'normal',//context normal or side
	'default'//priority on page order
  );
}
add_action( 'add_meta_boxes', 'dasi_add_instance_metaboxes' );


/**
 * Output the HTML for the metabox.
 */
function dasi_instances_options() {
  global $post;
  // Nonce field to validate form request came from current site
  wp_nonce_field( basename( __FILE__ ), 'instance_fields' );

  // Output the field
  include dirname(__DIR__) . '/views/metafields/instance_metafields.php';

}


/**
 * Save the metabox data
 */
function dasi_save_instances_meta( $post_id, $post ) {
	// Return if the user doesn't have edit permissions.
	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return $post_id;
	}

	// Verify this came from the our screen and with proper authorization,
	// because save_post can be triggered at other times.
	if(isset($_POST['instance_fields'])) {
		if ( ! wp_verify_nonce( $_POST['instance_fields'], basename(__FILE__) ) ) {
			return $post_id;
		}
	} else {
		return $post_id;
	}

	// Now that we're authenticated, time to save the data.
	// This sanitizes the data from the field and saves it into an array $instances_meta.
	$instances_meta['dasi_instance_id']				= esc_textarea( $_POST['dasi_instance_id'] );
	$instances_meta['dasi_instance_date']			= esc_textarea( $_POST['dasi_instance_date'] );
	$instances_meta['dasi_instance_time']			= esc_textarea( $_POST['dasi_instance_time'] );
	$instances_meta['dasi_instance_pricing']		= esc_textarea( $_POST['dasi_instance_pricing'] );
	$instances_meta['dasi_instance_genre']			= esc_textarea( $_POST['dasi_instance_genre'] );
	$instances_meta['dasi_instance_theme']			= esc_textarea( $_POST['dasi_instance_theme'] );
	$instances_meta['dasi_instance_location']		= esc_textarea( $_POST['dasi_instance_location'] );
	$instances_meta['dasi_instance_theatre']		= esc_textarea( $_POST['dasi_instance_theatre'] );
	$instances_meta['dasi_instance_age_range']		= esc_textarea( $_POST['dasi_instance_age_range'] );
	$instances_meta['dasi_instance_access_icons']	= esc_textarea( $_POST['dasi_instance_access_icons'] );

	// Cycle through the $instances_meta array.
	// Note, in this example we just have one item, but this is helpful if you have multiple.
	foreach ( $instances_meta as $key => $value ) :
		// Don't store custom data twice
		if ( 'revision' === $post->post_type ) {
		  return;
		}

		if ( get_post_meta( $post_id, $key, false ) ) {
		  // If the custom field already has a value, update it.
		  update_post_meta( $post_id, $key, $value );
		} else {
		  // If the custom field doesn't have a value, add it.
		  add_post_meta( $post_id, $key, $value);
		}

		if ( ! $value ) {
		  // Delete the meta key if there's no value
		  delete_post_meta( $post_id, $key );
		}
	endforeach;
}
add_action( 'save_post', 'dasi_save_instances_meta', 1, 2 );