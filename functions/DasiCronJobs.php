<?php


if (! wp_next_scheduled ( 'dasi_imports_event' )) {
	wp_schedule_event(time(), 'hourly', 'dasi_imports_event');
}

add_action('dasi_imports_event', 'dasi_imports');

function dasi_imports() {
	$dasi_event = new Dasi\DasiEvent();
	$dasi_event->import();

}