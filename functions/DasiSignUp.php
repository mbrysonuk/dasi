<?php

function DasiSignUp( $atts ){

		
	$api_key = get_option('dasi_spektrix_apikey');
	$dasi_clientname = get_option('dasi_spektrix_clientname');
	$ssl_cert_path = get_option('dasi_spektrix_crtpath');
	$ssl_key_path = get_option('dasi_spektrix_keypath');

?>


<?php 

	//get the tags via curl
    $tagcurl = curl_init();
    $tagoptions = array(
      CURLOPT_URL => "https://api.system.spektrix.com/".$dasi_clientname."/api/v2/tags?all=true&api_key=" . $api_key,
      CURLOPT_RETURNTRANSFER => 1,
      CURLOPT_SSLCERT => $ssl_cert_path,
      CURLOPT_SSLKEY => $ssl_key_path
    );
    curl_setopt_array($tagcurl, $tagoptions);

    $tagresponse = curl_exec($tagcurl);
    $tagresults = simplexml_load_string($tagresponse,"SimpleXMLElement", LIBXML_NOCDATA);
    curl_close($tagcurl);

?>

<form id="dasi_newsletter_signup" action="/wp-content/plugins/dasi/functions/post_newsletter.php?submit=true" method="POST">
  <input type="hidden" name="ReturnUrl" value="https://sunderlandculture.org.uk/newsletter-signup/?submit=true" />
  <textarea style="display: none" readonly id="dasi_xml" name="reqbody"></textarea><br />

  <label for="Email" class="show-for-sr">Email address:</label>
  <input id="dasi_Email" class="sc-input-group-field" name="Email" type="email" placeholder="Enter your email address" required />
  <input id="dasi_FirstName" class="sc-input-group-field" name="FirstName" type="text" placeholder="Enter your first name" required />
  <input id="dasi_LastName" class="sc-input-group-field" name="LastName" type="text" placeholder="Enter your last name" required />

  <div class="field-group">
    <div class="title">I would like to hear about;</div>
    <?php 
      $x = 0;
      foreach($tagresults as $result) { 
        if($result->TagGroup->attributes()->id == "404") { $x++;?>

          <input class="sc-input-group-field dasi_tags" name="<?php echo $result->attributes()->id; ?>" type="checkbox"/><?php echo $result->Name; ?><br> 

    <?php 
      } 
    } ?>

  </div>
  <input class="submit-btn" name="submit" type="submit" value="Subscribe" />
</form>

<script>
  jQuery('#dasi_newsletter_signup').change(function(){
    //reset it
    $('#dasi_xml').val('');
    //add start
    $('#dasi_xml').val('<NewCustomers><NewCustomer>');
    $('#dasi_xml').val($('#dasi_xml').val()+'<Email>'+$('#dasi_Email').val()+'</Email>');
    $('#dasi_xml').val($('#dasi_xml').val()+'<FirstName>'+$('#dasi_FirstName').val()+'</FirstName>');
    $('#dasi_xml').val($('#dasi_xml').val()+'<LastName>'+$('#dasi_LastName').val()+'</LastName>');

    //
    $('.dasi_tags').each(function(){
      if($(this).is(':checked') ) {
        $('#dasi_xml').val($('#dasi_xml').val()+"<Tag id=\'"+$(this).attr('name') +"\' />");
      }
    });

    $('#dasi_xml').val($('#dasi_xml').val() + '</NewCustomer></NewCustomers>');

  });
</script>


<?php
}
add_shortcode( 'DasiSignUp', 'DasiSignUp' );




function DasiSignUpMessages( $atts ){
	if(isset($_GET['response'])) {
		if(urldecode($_GET['response']) == "success") {
			echo '<div class="alert success">'.stripslashes($_GET['message']).'</div><style>.heading1 {display: none;}</style>';
		}else{
			echo '<div class="alert failure error">'.stripslashes($_GET['message']).'</div><style>.heading1 {display: none;}</style>';
		}
	}

}
add_shortcode( 'DasiSignUpMessages', 'DasiSignUpMessages' );