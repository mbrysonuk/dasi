<?php
namespace Dasi;

class DasiEvent {

	//create new event
	public function create($fields) {
		return wp_insert_post(wp_slash(
			array(
				'post_title'	=> $fields['dasi_event_title'],
				'post_slug'		=> sanitize_title($fields['dasi_event_title']),
				'post_type'		=> 'dasi_event',
				'post_status'	=> 'publish',
				'post_content'	=> $fields['dasi_event_description'],
				'meta_input'	=> $fields,	
			)
		));
	}

	//update event
	public function update($fields) {		
		$post_id = $fields['dasi_post_id'];
		foreach($fields as $key => $value) {
			if($key != 'dasi_post_id') {
				update_post_meta($post_id,$key,$value);				
			}
		};
	}

	public function import(){
		$curl = curl_init();
		$api_key = get_option('dasi_spektrix_apikey');
		$dasi_clientname = get_option('dasi_spektrix_clientname');
		$ssl_cert_path = get_option('dasi_spektrix_crtpath');
		$ssl_key_path = get_option('dasi_spektrix_keypath');
		$options = array(
			CURLOPT_URL => "https://system.spektrix.com/".$dasi_clientname."/api/v1/eventsrestful.svc/alltimes/allattributes/from?date=".date("Y-m-d").'&T=00:00:00',
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_SSLCERT => $ssl_cert_path,
			CURLOPT_SSLKEY => $ssl_key_path
		);
		curl_setopt_array($curl, $options);

		$response = curl_exec($curl);
		$results = simplexml_load_string($response,"SimpleXMLElement", LIBXML_NOCDATA);
		$fields = [];
		$attributesArr = [];

		// echo '<pre>';
		// var_dump($results);
		// echo '</pre>';

		if(!empty($results)) {
		foreach($results as $result) {	

			//handle the attributes into a useable format.
			$attributeArr = array();
			foreach($result->Attributes->EventAttribute as $attribute) {
				$att_name 	= $attribute->Name;
				$att_val 	= $attribute->Value;
				$att_name	= $att_name;
				$attributeArr[(string)$att_name] = (string)$att_val;
			}

			//Event Details
			$fields['dasi_event_id']			= (string)$result->Id;//event id
			$fields['dasi_event_title'] 		= (string)$result->Name;
			$fields['dasi_event_description'] 	= (string)$result->Description;
			$fields['dasi_is_spektrix']			= true;
			$fields['dasi_onsaleweb']			= (string)$result->OnSaleOnWeb;
			$fields['dasi_event_price_range']	= $attributeArr['Price Range'];//event pricing
			$fields['dasi_event_genre']			= $attributeArr['Genre'];//event genre
			$fields['dasi_event_theme']			= $attributeArr['Theme'];//event theme
			$fields['dasi_event_location']		= $attributeArr['Location'];//event location
			$fields['dasi_event_age_range']		= $attributeArr['Age Range'];//event theatre
			$fields['dasi_event_theatre']		= $attributeArr['Theatre Company'];//event theatre
			$fields['dasi_event_duration'] 		= (string)$result->Duration;//duration
			$fields['dasi_event_first_date']	= (string)$result->FirstInstance;//FirstInstance
			$fields['dasi_event_last_date']		= (string)$result->LastInstance;//LastInstance
			
			

			//check to see wether a record needs adding, or updating.
			$exists_check = $this->exists((string)$fields['dasi_event_id']);
			if(!$exists_check) {
				echo 'creating event: '.$fields['dasi_event_id'].' ...<br>';
				echo $this->create($fields);
			}else{
				echo 'updating event: '.$fields['dasi_event_id'].' ...<br>';
				$fields['dasi_post_id'] = $exists_check;
				$this->update($fields);
			}
			
			//handle the instances
			$instances = new DasiInstance();
			$instances->import(
				$result->Times->EventTime,
				$fields['dasi_event_title'],
				$fields['dasi_event_id']);	
		}
		}

		echo 'Complete!';
	}

	//check to see if event exists
	private function exists($dasi_event_id) {

		$exists_args = array(
			'post_type'			=> 'dasi_event',
			'posts_per_page'	=> 1,
			'meta_key'			=> 'dasi_event_id',
			'meta_value'		=> $dasi_event_id
		);
		$exists_query = new \WP_Query($exists_args);

		if($exists_query->have_posts()) {
			//return the POST ID for updating.
			return $exists_query->posts[0]->ID;
		}else{
			return false;
		}
	}
}