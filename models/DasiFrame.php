<?php

namespace Dasi;


class DasiFrame {

	function __construct() {
		
	}

	public static function SetIframeHead() {
		$dasi_clientname = get_option('dasi_spektrix_clientname');
		echo "<script
		 type='text/javascript'
		 src='https://system.spektrix.com/".$dasi_clientname."/website/scripts/resizeiframe.js'>
		</script>";
	}

	public static function ConstructIframe($path) {
		$dasi_clientname = get_option('dasi_spektrix_clientname');
		return '<iframe frameborder="0" 
		 		name="SpektrixIFrame"
 				id="SpektrixIFrame"
 				frameborder="0"
				src="https://system.spektrix.com/'.$dasi_clientname.$path.'"
				style="width: 100%; height: 1000px;"
				onload="setTimeout(function(){ window.scrollTo(0,0);}, 100)"
				>
			</iframe>';
	}
}