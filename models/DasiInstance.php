<?php

namespace Dasi;

class DasiInstance {

	//create new instance
	public function create($fields) {
		wp_insert_post(
			array(
				'post_title'	=> $fields['dasi_event_title'],// . " - " . $fields['dasi_instance_start_date'],
				'post_slug'		=> sanitize_title($fields['dasi_event_title'] . $fields['dasi_instance_start_date']),
				'post_status'	=> 'publish',
				'post_type'		=> 'dasi_instance',
				'post_content'	=> '',//instances dont have content
				'meta_input'	=> $fields
			)
		);
	}

	//update instance
	public function update($fields) {
		$post_id = $fields['dasi_post_id'];
		foreach($fields as $key => $value) {
			if($key != 'dasi_post_id') {
				update_post_meta($post_id,$key,$value);				
			}
		};
	}

	//check to see if event exists
	private function exists($dasi_instance_id) {
		$exists_args = array(
			'post_type'			=> 'dasi_instance',
			'posts_per_page'	=> 1,
			'meta_key'			=> 'dasi_instance_id',
			'meta_value'		=> $dasi_instance_id
		);
		$exists_query = new \WP_Query($exists_args);

		if($exists_query->have_posts()) {
			return $exists_query->posts[0]->ID;
		}else{
			return false;
		}
	}

	public function import($instances, $event_name, $event_id){

		$fields = [];
		$attributesArr = [];

		foreach($instances as $result) {	

			//Instance Attributes - handle the attributes into a useable format.
			$attributeArr = array();
			foreach($result->Attributes->EventAttribute as $attribute) {
				$att_name 	= $attribute->Name;
				$att_val 	= $attribute->Value;
				$att_name	= $att_name;
				$attributeArr[(string)$att_name] = (string)$att_val;
			}

			$fields['dasi_event_title']				= $event_name;
			$fields['dasi_related_event_id']		= $event_id;

			$fields['dasi_instance_id']				= (string)$result->EventInstanceId;
			$fields['dasi_is_spektrix']				= true;

			$fields['dasi_icon_wheelchairaccess']	= $attributeArr['Wheelchair Accessible'];
			$fields['dasi_icon_captioned']			= $attributeArr['Captioned'];
			$fields['dasi_icon_bslinterpreted']		= $attributeArr['BSL Interpreted'];
			$fields['dasi_icon_audiodescription']	= $attributeArr['Audio Description Available'];
			$fields['dasi_instance_capacity']		= (string)$result->Capacity;
			$fields['dasi_instance_seatsavailable']	= (string)$result->SeatsAvailable;	


			//format the date and time into useable format
			$dateval = (string)$result->Time;
			$date = date('Y-m-d',strtotime($dateval));
			$time = date('H:i:s',strtotime($dateval));
			$fields['dasi_instance_start_date']	= $date;
			$fields['dasi_instance_start_time']	= $time;

			//check to see wether a record needs adding, or updating.
			$exists_check = $this->exists($fields['dasi_instance_id']);
			if(!$exists_check) {
				echo 'creating instance: '.$fields['dasi_instance_id'].' ...<br>';
				echo $this->create($fields);
			}else{
				echo 'updating instance: '.$fields['dasi_instance_id'].' ...<br>';
				$fields['dasi_post_id'] = $exists_check;
				$this->update($fields);
			}
		}

		//return the availability in this response, calculated from above.
	}
}