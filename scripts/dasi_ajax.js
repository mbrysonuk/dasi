//date picker customisation options
$(document).ready(function() {
    $("#dasi-filter-date").daterangepicker({
      autoUpdateInput: false,
      opens: 'left',
      "locale": {
        "format": "DD/MM/YYYY",
      }
    
  }, function(start, end, label) {
    console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
    $('#dasi-filter-date-start').attr('value',start.format('YYYY-MM-DD'));
    $('#dasi-filter-date-end').attr('value',end.format('YYYY-MM-DD'));

  });

  $("#dasi-filter-date").on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('DD MMM YYYY') + ' - ' + picker.endDate.format('DD MMM YYYY'));
  });

  $("#dasi-filter-date").on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    $('#dasi-filter-date-start').attr('value','');
    $('#dasi-filter-date-end').attr('value','');
  });
});



//load more posts
jQuery('.dasi_loadmore').click(function($) {

  var dasi_events = document.getElementById('dasi-events');
  jQuery(dasi_events).attr('data-pagenum',(parseInt(jQuery(dasi_events).attr('data-pagenum')) + 1) );
  jQuery(this).hide();

  let dasi_location 	= dasi_events.getAttribute('data-location'),
    dasi_startdate 		= dasi_events.getAttribute('data-startdate'),
    dasi_enddate 		= dasi_events.getAttribute('data-enddate'),
    dasi_genre 			= dasi_events.getAttribute('data-genre'),
    dasi_theme 			= dasi_events.getAttribute('data-theme'),
    dasi_numposts 		= dasi_events.getAttribute('data-numposts'),
    dasi_pagenum		= dasi_events.getAttribute('data-pagenum'),
    dasi_postsperpage 	= dasi_events.getAttribute('data-postsperpage'),
    dasi_pagesmax	 	= dasi_events.getAttribute('data-pagesmax');

    //Some event will trigger the ajax call, you can push whatever data to the server, simply passing it to the "data" object in ajax call
    jQuery.ajax({
      url: ajax_object.ajaxurl, // this is the object instantiated in wp_localize_script function
      type: 'POST',
      data: {
        action: 'dasiloadmore', // this is the function in your functions.php that will be triggered
        dasi_location: dasi_location,
        dasi_theme: dasi_theme,
        dasi_genre: dasi_genre,
        dasi_pagenum: dasi_pagenum,
        dasi_postsperpage: dasi_postsperpage,
        dasi_startdate: dasi_startdate,
        dasi_enddate: dasi_enddate

      },
      success: function(data) {
        //Do something with the result from server
        
        if(data == false ) {
        	console.log('false');
        }else{
        	console.log('notfalse');
	        jQuery(dasi_events).append(data );
        }

        //show the button if more content available
        if(dasi_pagesmax > dasi_pagenum) {
        	jQuery('.dasi_loadmore').show();	
        }
      }
    });
});


//wipe the results and re add them with the filters.
jQuery('#dasi-filter').click(function($) {
  event.preventDefault();



  var dasi_events = document.getElementById('dasi-events');
  dasi_events.setAttribute('data-pagenum',1);

  //set the containing div attributes based on INPUT values

  //start date
  //end date
  let dasi_filter_date_start = document.getElementById('dasi-filter-date-start').value;
  	dasi_filter_date_end = document.getElementById('dasi-filter-date-end').value;


  	dasi_events.setAttribute('data-startdate',dasi_filter_date_start);
  	dasi_events.setAttribute('data-enddate',dasi_filter_date_end);
  	

  //theme
  let dasi_filter_theme = document.getElementById('dasi-filter-theme').value;
  if(dasi_filter_theme != 'all-themes' && dasi_filter_theme != '') {
  	dasi_events.setAttribute('data-theme',dasi_filter_theme);
  }else{
  	dasi_events.setAttribute('data-theme','');
  }
  //theme
  let dasi_filter_genre = document.getElementById('dasi-filter-genre').value;
  if(dasi_filter_genre != 'all-genres' && dasi_filter_genre != '') {
  	dasi_events.setAttribute('data-genre',dasi_filter_genre);
  }else{
  	dasi_events.setAttribute('data-genre','');
  }




  //get the attributes from the containing div
  let dasi_location		= dasi_events.getAttribute('data-location'),
      dasi_startdate	= dasi_events.getAttribute('data-startdate'),
      dasi_enddate 		= dasi_events.getAttribute('data-enddate'),
      dasi_genre 		= dasi_events.getAttribute('data-genre'),
      dasi_theme 		= dasi_events.getAttribute('data-theme'),
      dasi_numposts		= dasi_events.getAttribute('data-numposts'),
      dasi_pagenum		= dasi_events.getAttribute('data-pagenum'),
      dasi_postsperpage = dasi_events.getAttribute('data-postsperpage'),
      dasi_pagesmax	 	= dasi_events.getAttribute('data-pagesmax');

    //Some event will trigger the ajax call, you can push whatever data to the server, simply passing it to the "data" object in ajax call
    jQuery.ajax({
      url: ajax_object.ajaxurl, // this is the object instantiated in wp_localize_script function
      type: 'POST',
      data: {
        action: 'dasiloadmore', // this is the function in your functions.php that will be triggered
        dasi_location: dasi_location,
        dasi_theme: dasi_theme,
        dasi_genre: dasi_genre,
        dasi_pagenum: dasi_pagenum,
        dasi_postsperpage: dasi_postsperpage,
        dasi_startdate: dasi_startdate,
        dasi_enddate: dasi_enddate,
        dasi_reset_posts: true,
        dasi_is_ajax_reload: true

      },
      success: function(data) {
        //Do something with the result from server
        //always append, even when its empty.
        var resp =  JSON.parse(data);
        jQuery(dasi_events).html(resp.data);
        dasi_events.setAttribute('data-pagenum',1);
        dasi_events.setAttribute('data-pagesmax',resp.maxpages);
        jQuery('.dasi_loadmore').hide(); 
        //show the button if more content available
        if(resp.maxpages > 1) {

          jQuery('.dasi_loadmore').show();  
        }
      }
    });
});


//dasi instance filter

//load more posts
jQuery('.dasi_i_loadmore').click(function($) {

  var dasi_instances = document.getElementById('dasi-instances');
  jQuery(dasi_instances).attr('data-pagenum',(parseInt(jQuery(dasi_instances).attr('data-pagenum')) + 1) );
  jQuery(this).hide();

  let dasi_startdate    = dasi_instances.getAttribute('data-startdate'),
    dasi_enddate        = dasi_instances.getAttribute('data-enddate'),
    dasi_numposts       = dasi_instances.getAttribute('data-numposts'),
    dasi_pagenum        = dasi_instances.getAttribute('data-pagenum'),
    dasi_postsperpage   = dasi_instances.getAttribute('data-postsperpage'),
    dasi_pagesmax       = dasi_instances.getAttribute('data-pagesmax');
    dasi_event_id       = dasi_instances.getAttribute('dasa-event-id');

    //Some event will trigger the ajax call, you can push whatever data to the server, simply passing it to the "data" object in ajax call
    jQuery.ajax({
      url: ajax_object.ajaxurl, // this is the object instantiated in wp_localize_script function
      type: 'POST',
      data: {
        action: 'dasiinstanceloadmore', // this is the function in your functions.php that will be triggered
        dasi_event_id: dasi_event_id,
        dasi_pagenum: dasi_pagenum,
        dasi_postsperpage: dasi_postsperpage,
        dasi_startdate: dasi_startdate,
        dasi_enddate: dasi_enddate

      },
      success: function(data) {
        //Do something with the result from server
        
        if(data == false ) {
          console.log('false');
        }else{
          console.log('notfalse');
          jQuery(dasi_instances).append(data);
        }

        //show the button if more content available
        if(dasi_pagesmax > dasi_pagenum) {
          jQuery('.dasi_i_loadmore').show();  
        }
      }
    });
});